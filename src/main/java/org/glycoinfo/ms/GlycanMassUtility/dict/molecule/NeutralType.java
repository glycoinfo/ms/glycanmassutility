package org.glycoinfo.ms.GlycanMassUtility.dict.molecule;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;
import org.glycoinfo.ms.GlycanMassUtility.om.Molecule;

public enum NeutralType implements IMassElement {

	Water(   "hydration", "H2O"),
	Hydrogen("hydrogen",  "H"),

	// Modification
	Oxy  ("oxygenation",   "O"),
	Hyd  ("hydrogenation", "H2"),
	;

	private String name;
	private Molecule molecule;

	private NeutralType(String a_name, String a_strFormula) {
		this.name = a_name;
		this.molecule = Molecule.parse(a_strFormula);
	}

	public String getName() {
		return this.name;
	}

	public Molecule getMolecule() {
		return this.molecule;
	}

	@Override
	public BigDecimal getMass() {
		return this.molecule.getMass();
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.molecule.getMonoisotopicMass();
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.molecule.getAverageMass();
	}

	/**
	 * Return the NeutralType with name {@code a_name}
	 * @param a_name String of name
	 * @return NeutralType with the name
	 */
	public static NeutralType forName(String a_name) {
		for ( NeutralType e : NeutralType.values() )
			if ( e.name().equals(a_name) )
				return e;
		return null;
	}
}
