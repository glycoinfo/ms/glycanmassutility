package org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide;

import java.util.HashMap;
import java.util.Map;

public class MonosaccharideDictionary {

	private static final Map<String, MonosaccharideType> mapNameToMS = new HashMap<>();
	static {
		for ( MonosaccharideList e : MonosaccharideList.values() ) {
			mapNameToMS.put(e.getType().getName(), e.getType());
			for ( String synonym : e.getType().getSynonyms() )
				mapNameToMS.put(synonym, e.getType());
		}
	}

	public static boolean findMonosaccharide(String strName) {
		return mapNameToMS.containsKey(strName);
	}

	public static MonosaccharideType getMonosaccharide(String strName) {
		return mapNameToMS.get(strName);
	}

}
