package org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideType.CoreModification;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideType.Substituent;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.SubstituentList;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.SubstituentType;

public enum MonosaccharideList {
	_Pen      (null,       5, 1, 6, null,      null,    "Pentose"),
	_Hex      (null,       6, 1, 6, null,      null,    "Hexose"),
	_Hep      (null,       7, 1, 6, null,      null,    "Heptose"),
	_PenN     (null,       5, 1, 6, null,      "2:N",   "Pentosamine"),
	_2dPen    (null,       5, 1, 6, "2:d",     null,    "2-deoxy Pentose"),
	_4dPen    ("dPen",     5, 1, 6, "4:d",     null,    "4-deoxy Pentose"),
	_4dPenN   (null,       5, 1, 6, "4:d",     "2:N",   "4-deoxy Pentosamine"),
	_HexN     (null,       6, 1, 6, null,      "2:N",   "Hexosamine"),
	_HexNAc   (null,       6, 1, 6, null,      "2:NAc", "N-acetyl Hexosamine"),
	_dHex     ("6dHex",    6, 1, 6, "6:d",     null,    "6-deoxy Hexose"),
	_ddHex    (null,       6, 1, 6, "4:d,6:d", null,    "4,6-deoxy Hexose"),
	_HexA     (null,       6, 1, 6, "6:a",     null,    "Hexuronic acid"),
	_4uHexA   ("uHexA",    6, 1, 6, "4:u,6:a", null,    "4-unsaturated Hexuronic acid"),
	_4MeHex   ("MeHex",    6, 1, 6, null,      "4:Me",  "4-methyl Hexose"),
	_6dHep    ("dHep",     7, 1, 6, "6:d",     null,    "6-deoxy Heptose"),
	_MurNAc   (null,       6, 1, 6, null,      "2:NAc,3:Lac", "N-acetyl Muramic acid"),
	_KDO      (null,       8, 2, 6, "1:a,3:d", null,    "KDO"),
	_KDN      (null,       9, 2, 6, "1:a,3:d", null,    "KDN"),
	_KO       (null,       8, 2, 6, "1:a",     null,    "KO"),
	_Neu      (null,       9, 2, 6, "1:a,3:d", "5:N",   "Neuraminic acid"),
	_Neu5Ac   ("NeuAc",    9, 2, 6, "1:a,3:d", "5:NAc", "N-acetyl Neuraminic acid"),
	_Neu5Gc   ("NeuGc",    9, 2, 6, "1:a,3:d", "5:NGc", "N-glycolyl Neuraminic acid"),
	_Neu5AcLac("NeuAcLac", 9, 2, 6, "1:a,3:d,?:lac", "5:NAc", "Lactonized Neu5Ac"),
	_Neu5GcLac("NeuGcLac", 9, 2, 6, "1:a,3:d,?:lac", "5:NGc", "Lactonized Neu5Gc"),
	_Neu5Ac38 ("3-8NeuAc", 9, 2, 6, "1:a,3:d", "1:NMe,5:NAc",  "a2,3/8- Neu5Ac (SALSA)"),
	_Neu5Ac6  ("6NeuAc",   9, 2, 6, "1:a,3:d", "1:NiPr,5:NAc", "a2,6- Neu5Ac (SALSA)"),
	_Neu5Gc38 ("3-8NeuGc", 9, 2, 6, "1:a,3:d", "1:NMe,5:NGc",  "a2,3/8- Neu5Gc (SALSA)"),
	_Neu5Gc6  ("6NeuGc",   9, 2, 6, "1:a,3:d", "1:NiPr,5:NGc", "a2,6- Neu5Gc (SALSA)"),
	_anHex    (null,       6, 1, 6, "?:an",    null,    "Anhydro Hexose"),
	;

	private MonosaccharideType type;

	private MonosaccharideList(String strSynonym, int iLength, int iAnomPos, int iRingSize,
			String strCoreModifs, String strSubsts, String strDescription) {

		List<String> lSynonyms = Arrays.asList(
				(strSynonym != null)? strSynonym.split(",") : new String[] {}
			);
		AnomerType anomer = (iRingSize > 2) ? AnomerType.x : AnomerType.o;
		List<CoreModification> lMods = parseModifications(strCoreModifs);
		List<Substituent> lSubsts = parseSubstituentsInMonosaccharideType(strSubsts);

		type = new MonosaccharideType(this.name().substring(1), lSynonyms,
				iLength, anomer, iAnomPos, iRingSize, lMods, lSubsts, strDescription);
	}

	public MonosaccharideType getType() {
		return this.type;
	}

	private static Map<String, List<CoreModification>> mapNameToCoreModifs;
	private static Map<String, List<Substituent>> mapNameToSubsts;

	private static List<CoreModification> parseModifications(String strMods) {
		if (strMods == null)
			return new ArrayList<>();

		if ( mapNameToCoreModifs == null )
			mapNameToCoreModifs = new HashMap<>();
		if ( mapNameToCoreModifs.containsKey(strMods) )
			return mapNameToCoreModifs.get(strMods);

		List<CoreModification> lMods = new ArrayList<>();
		String[] tokens = strMods.split(",");
		for (String token : tokens) {
			String[] strMod = token.split(":");
			int pos = (strMod[0].equals("?"))? -1 : Integer.valueOf(strMod[0]);
			CoreModificationType type = CoreModificationType.forName(strMod[1]);
			lMods.add(new CoreModification(type, pos));
		}

		mapNameToCoreModifs.put(strMods, lMods);
		return lMods;
	}

	private static List<Substituent> parseSubstituentsInMonosaccharideType(String strSubsts) {
		if (strSubsts == null)
			return new ArrayList<>();

		if ( mapNameToSubsts == null )
			mapNameToSubsts = new HashMap<>();
		if ( mapNameToSubsts.containsKey(strSubsts) )
			return mapNameToSubsts.get(strSubsts);

		List<Substituent> lSubsts = new ArrayList<>();
		String[] tokens = strSubsts.split(",");
		for (String token : tokens) {
			String[] strMod = token.split(":");
			int pos = (strMod[0].equals("?"))? -1 : Integer.valueOf(strMod[0]);
			SubstituentType type = SubstituentList.valueOf(strMod[1]).getType();
			lSubsts.add(new Substituent(type, pos));
		}

		mapNameToSubsts.put(strSubsts, lSubsts);
		return lSubsts;
	}

}
