package org.glycoinfo.ms.GlycanMassUtility.dict.atom;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An dictionary class for atoms with their masses.
 * @author Masaaki Matsubara
 *
 */
public enum AtomType implements IAtom {

	e ("e",  "electron",    "0.0005486",    "0.0005486"),
	H ("H",  "Hydrogen",    "1.007825032",  "1.007941"),
	D ("D",  "Deuterium",   "2.014101778",  "2.014101778"),
	Li("Li", "Lithium",     "7.016004000",  "6.940037"),
	C ("C",  "Carbon",     "12.000000000", "12.010736"),
	N ("N",  "Nitrogen",   "14.003074005", "14.006743"),
	O ("O",  "Oxygen",     "15.994914622", "15.999405"),
	Na("Na", "Sodium",     "22.989769670", "22.989769670"),
	P ("P",  "Phosphorus", "30.973761510", "30.973761510"),
	S ("S",  "Sulfur",     "31.972070690", "32.066085"),
	Cl("Cl", "Chlorine",   "34.968852710", "35.452538"),
	K ("K",  "Potassium",  "38.963706900", "39.098301"),
	F ("F",  "Fluoro",     "18.998403227", "18.9984032"),
	Br("Br", "Bromo",      "78.918337122", "79.904"),
	I ("I",  "Iodine",    "126.904473",   "126.9044734"),
	;

	private static Logger logger = LoggerFactory.getLogger(AtomType.class);

	private String symbol;
	private String name;
	private BigDecimal main_mass;
	private BigDecimal avg_mass;

	private AtomType(String a_symbol, String a_name, String a_main_mass, String a_avg_mass) {
		this.symbol = a_symbol;
		this.name = a_name;
		this.main_mass = new BigDecimal(a_main_mass);
		this.avg_mass = new BigDecimal(a_avg_mass);
	}

	@Override
	public String getSymbol() {
		return this.symbol;
	}

	@Override
	public String getAtomSymbol() {
		return this.symbol;
	}

	/**
	 * Return the name of this atom.
	 */
	public String getName() {
		return this.name;
	}

	@Override
	public BigDecimal getMass() {
		return this.main_mass;
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.main_mass;
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.avg_mass;
	}

	/**
	 * Return AtomType with the given symbol
	 * @param a_symbol String of atom symbol
	 * @return AtomType with the given symbol
	 */
	public static AtomType forSymbol(String a_symbol) {
		for ( AtomType e : AtomType.values() )
			if ( e.symbol.equals(a_symbol) )
				return e;
		logger.error("No atom with the symbol: {}", a_symbol);
		return null;
	}
}
