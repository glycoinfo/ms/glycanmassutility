package org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class SubstituentTypeCustum extends SubstituentType {

	private final int nScale = 4;

	SubstituentTypeCustum(String name, BigDecimal bdMass, boolean bIsRedEnd) {
		super(name, new ArrayList<>(),
				"?", 1, false, 1, false, 1, false, bIsRedEnd,
				"Custom reducing end type: "+name);

		this.m_dMonoisotopicMass = bdMass;
		this.m_dAverageMass = bdMass;

	}

	public String toString() {
		BigDecimal bdMass = this.m_dMonoisotopicMass;
		return this.getName()+"="+bdMass.setScale(nScale, RoundingMode.HALF_UP);
	}

}
