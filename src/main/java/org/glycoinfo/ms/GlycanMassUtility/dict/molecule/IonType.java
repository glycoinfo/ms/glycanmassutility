package org.glycoinfo.ms.GlycanMassUtility.dict.molecule;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.om.IIonElement;
import org.glycoinfo.ms.GlycanMassUtility.om.Molecule;

public enum IonType implements IIonElement {

	H    ("H+"),
	Li   ("Li+"),
	Na   ("Na+"),
	K    ("K+"),
	Cl   ("Cl-"),
	H2PO4("H2PO4-"),
	SO4  ("SO4--"),
	;

	private String strFormula;
	private Molecule molIon;

	private IonType(String a_strFormula) {
		this.strFormula = a_strFormula.replace("+", "").replace("-", "");
		this.molIon = Molecule.parse(a_strFormula);
	}

	public String getName() {
		return this.strFormula;
	}

	public int getCharges() {
		return this.molIon.getCharges();
	}

	@Override
	public BigDecimal getMass() {
		return this.molIon.getMonoisotopicMass();
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.molIon.getMonoisotopicMass();
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.molIon.getAverageMass();
	}

	/**
	 * Return the IonType with name {@code a_name}
	 * @param a_name String of name
	 * @return IonType with the name
	 */
	public static IonType forName(String a_name) {
		for ( IonType e : IonType.values() )
			if ( e.strFormula.equals(a_name) )
				return e;
		return null;
	}
}
