package org.glycoinfo.ms.GlycanMassUtility.dict.atom;

import java.util.Comparator;

import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;

public interface IAtom extends IMassElement {

	public static final Comparator<IAtom> SymbolComparator = new Comparator<IAtom>() {
		@Override
		public int compare(IAtom o1, IAtom o2) {
			return o1.getSymbol().compareTo(o2.getSymbol());
		}
	};

	public String getSymbol();
	/**
	 * Return the chemical symbol of this atom.
	 */
	public String getAtomSymbol();

}
