package org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent;

public enum SubstituentList {

	Ac   ("C2H4O2",    1, true,  1, false, 1, false, "O-acetyl"),
	N    ("NH3",       3, false, 2, false, 2, false, "Amine"),
	NAc  ("C2H5NO",    2, false, 1, false, 1, false, "N-acetyl"),
	NGc  ("C2H5NO2",   3, false, 2, false, 2, false, "N-glycolyl"),
	Me   ("CH4O",      1, false, 1, false, 1, false, "O-methyl"),
	Lac  ("C3H6O3",    2, false, 2, false, 1, true,  "Lactate"),
	Pyr  ("C3H4O3",    1, false, 1, true,  1, true,  "Pyruvate"),
	P    ("H3PO4",     3, false, 1, false, 1, true,  "Phosphate"),
	S    ("H2SO4",     1, false, 1, false, 1, true,  "Sulfate"),
	PC   ("C5H14NO4P", 2, false, 1, false, 1, false, "Phosphocoline"),
	PPEtn("C2H9NO7P2", 2, false, 1, false, 1, false, "Diphosphoethanolamine"),
	PEtn ("C2H8NO4P",  2, false, 1, false, 1, false, "Phosphoethanolamine"),
	NMe  ("CH5N",      2, false, 1, false, 1, false, "N-methyl"),
	NiPr ("C3H9N",     2, false, 1, false, 1, false, "Isopropylamine"),
	;

	private SubstituentType type;

	private SubstituentList(String strComp,
			int nMe, boolean dropMe, int nAc, boolean dropAc,
			int nLink, boolean isAcid, String strDescription) {

		this.type = new SubstituentType(this.toString(), null,
				strComp, nMe, dropMe, nAc, dropAc,
				nLink, isAcid, false, strDescription);
	}

	public SubstituentType getType() {
		return type;
	}
}
