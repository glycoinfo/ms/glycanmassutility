package org.glycoinfo.ms.GlycanMassUtility.dict.generic;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;

public class GenericNeutral implements IMassElement {

	private String name;
	private BigDecimal bdMass;

	GenericNeutral(String a_name, BigDecimal a_bdMass) {
		this.name = a_name;
		this.bdMass = a_bdMass;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public BigDecimal getMass() {
		return this.bdMass;
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.bdMass;
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.bdMass;
	}

}
