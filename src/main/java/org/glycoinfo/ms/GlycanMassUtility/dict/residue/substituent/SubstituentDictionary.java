package org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubstituentDictionary {

	private static final Logger logger = LoggerFactory.getLogger(SubstituentDictionary.class);

	public static final Map<String, SubstituentType> mapNameToSubst = new HashMap<>();
	public static final Map<String, SubstituentType> mapNameToRedEnd = new HashMap<>();
	static {
		for ( SubstituentList e : SubstituentList.values() )
			mapNameToSubst.put(e.getType().getName(), e.getType());
		for ( ReducingEndList e : ReducingEndList.values() )
			mapNameToRedEnd.put(e.getType().getName(), e.getType());
	}

	public static boolean addCustumSubstituent(String name, String strMass, boolean isRedEnd) {
		if ( !isRedEnd && mapNameToSubst.containsKey(name) ) {
			logger.warn("The substituent name {} is already used.", name);
			return false;
		}
		if ( isRedEnd && mapNameToRedEnd.containsKey(name) ) {
			logger.warn("The reducing end name {} is already used.", name);
			return false;
		}

		try {
			BigDecimal bdMass = new BigDecimal(strMass);
			mapNameToSubst.put(name, new SubstituentTypeCustum(name, bdMass, isRedEnd) );
		} catch ( NumberFormatException e ) {
			logger.warn("Invalid mass value: {}", strMass);
			return false;
		}
		return true;
	}

	public static boolean removeCustumSubstituent(String name) {
		if (mapNameToSubst.containsKey(name)) {
			mapNameToSubst.remove(name);
			return true;
		}
		if (mapNameToRedEnd.containsKey(name)) {
			mapNameToRedEnd.remove(name);
			return true;
		}
		logger.warn("The substituent name {} is not found.", name);
		return false;
	}

	public static boolean findSubstituent(String strName) {
		return mapNameToSubst.containsKey(strName);
	}

	public static SubstituentType getSubstituent(String strName) {
		return mapNameToSubst.get(strName);
	}

	public static boolean findReducingEnd(String strName) {
		return mapNameToRedEnd.containsKey(strName);
	}

	public static SubstituentType getReducingEnd(String strName) {
		return mapNameToRedEnd.get(strName);
	}
}
