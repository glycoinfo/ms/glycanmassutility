package org.glycoinfo.ms.GlycanMassUtility.dict.generic;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.glycoinfo.ms.GlycanMassUtility.dict.molecule.IonType;
import org.glycoinfo.ms.GlycanMassUtility.dict.molecule.NeutralType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for adding and providing a user-specified ions and neutral molecules.
 * When an element to add has the name which is already used in this system
 * (neutral molecules for {@link NeutralType} or ions for {@link IonType}),
 * the element cannot be added.
 * @see NeutralType
 * @see IonType
 * @author Masaaki Matsubara
 *
 */
public class GenericMassElementDictionary {

	private static final Logger logger = LoggerFactory.getLogger(GenericMassElementDictionary.class);

	private static final Map<String, GenericNeutral> mapNameToNeutral = new TreeMap<>();
	private static final Map<String, GenericIon> mapNameToIon = new TreeMap<>();

	public static boolean addGenericNeutral(String a_name, String a_strMass) {
		// Do not add if the name is used in NeutralType
		if ( NeutralType.forName(a_name) != null ) {
			logger.error("The name {} is already used in this system.", a_name);
			return false;
		}
		addGenericElement(a_name, a_strMass, 0);
		return true;
	}

	public static boolean addGenericIon(String a_name, String a_strMass, int a_iCharges) {
		// Do not add when the charge is zero
		if ( a_iCharges == 0 ) {
			logger.error("An ion without charge cannot be added.");
			return false;
		}
		// Do not add when the name is used in IonType
		if ( IonType.forName(a_name) != null ) {
			logger.error("The name {} is already used in this system.", a_name);
			return false;
		}
		// TODO: check the name can be parsed in IonCloud.getInstance(String)

		addGenericElement(a_name, a_strMass, a_iCharges);
		return true;
	}

	private static void addGenericElement(String a_name, String a_strMass, int a_iCharges) {
		if ( a_name == null || a_strMass == null )
			return;

		// Check if it is already added
		String type = ( a_iCharges == 0 )? "neutral" : "ion";
		Set<String> setNames = ( a_iCharges == 0 )?
			mapNameToNeutral.keySet() : mapNameToIon.keySet();
		if ( setNames.contains(a_name) ) {
			logger.warn("The {} with the same name {} cannot be added.", type, a_name);
			return;
		}

		// Check mass
		BigDecimal mass = null;
		try {
			mass = new BigDecimal(a_strMass);
		} catch (NumberFormatException e) {
			logger.error("Invalid mass number: {}", a_strMass);
			return;
		}

		// Add object
		if ( a_iCharges == 0 ) {
			GenericNeutral elem = new GenericNeutral(a_name, mass);
			mapNameToNeutral.put(a_name, elem);
		} else {
			GenericIon ion = new GenericIon(a_name, mass, a_iCharges);
			mapNameToIon.put(a_name, ion);
		}
	}

	public static GenericIon getIon(String a_name) {
		if ( a_name == null || !mapNameToIon.containsKey(a_name) )
			return null;
		return mapNameToIon.get(a_name);
	}

	public static GenericNeutral getNeutral(String a_name) {
		if ( a_name == null || !mapNameToNeutral.containsKey(a_name) )
			return null;
		return mapNameToNeutral.get(a_name);
	}

}
