package org.glycoinfo.ms.GlycanMassUtility.dict.generic;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.om.IIonElement;

public class GenericIon extends GenericNeutral implements IIonElement {

	private int iCharge;

	GenericIon(String a_name, BigDecimal a_bdMass, int a_iCharges) {
		super(a_name, a_bdMass);
		this.iCharge = a_iCharges;
	}

	@Override
	public int getCharges() {
		return this.iCharge;
	}
}
