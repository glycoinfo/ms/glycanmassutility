package org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment;

public interface ICleavageType {
	public char getSymbol();
	public boolean isCrossRing();
	public boolean isRootSide();
	public boolean hasOxygen();
}
