package org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment;

import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;

public interface IFragmentType extends IMassElement {
	public ICleavageType getCleavageType();

	public int getMaxLinkages();
	public int getNumMethylations();
	public int getNumAcethylations();
}