package org.glycoinfo.ms.GlycanMassUtility.dict.atom;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum IsotopeType implements IAtom {

	H_1  (AtomType.H,   1, true,   "1.007825032", "0.99988500"),
	H_2  (AtomType.H,   2, true,   "2.014101778", "0.00011500"),
	H_3  (AtomType.H,   3, false,  "3.016049268", "0.00000000"),
	D    (AtomType.D,   2, true,   "2.014101778", "1.00000000"),
	Li_6 (AtomType.Li,  6, true,   "6.015122300", "0.07590000"),
	Li_7 (AtomType.Li,  7, true,   "7.016004000", "0.92410000"),
	C_12 (AtomType.C,  12, true,  "12.000000000", "0.98930000"),
	C_13 (AtomType.C,  13, true,  "13.003354838", "0.01070000"),
	C_14 (AtomType.C,  14, false, "14.003241988", "0.00000000"),
	N_14 (AtomType.N,  14, true,  "14.003074005", "0.99632000"),
	N_15 (AtomType.N,  15, true,  "15.000108898", "0.00368000"),
	O_16 (AtomType.O,  16, true,  "15.994914622", "0.99757000"),
	O_17 (AtomType.O,  17, true,  "16.999131500", "0.00038000"),
	O_18 (AtomType.O,  18, true,  "17.999160400", "0.00205000"),
	Na_23(AtomType.Na, 23, true,  "22.989769670", "1.00000000"),
	P_31 (AtomType.P,  31, true,  "30.973761510", "1.00000000"),
	S_32 (AtomType.S,  32, true,  "31.972070690", "0.94930000"),
	S_33 (AtomType.S,  33, true,  "32.971458500", "0.00760000"),
	S_34 (AtomType.S,  34, true,  "33.967866830", "0.04290000"),
	S_36 (AtomType.S,  36, true,  "35.967080880", "0.00020000"),
	Cl_35(AtomType.Cl, 35, true,  "34.968852710", "0.75780000"),
	Cl_37(AtomType.Cl, 37, true,  "36.965902600", "0.24220000"),
	K_39 (AtomType.K,  39, true,  "38.963706900", "0.93258100"),
	K_40 (AtomType.K,  40, false, "39.963998670", "0.00011700"),
	K_41 (AtomType.K,  41, true,  "40.961825970", "0.06730200"),
	;

	private static Logger logger = LoggerFactory.getLogger(IsotopeType.class);

	private AtomType atomType;
	private boolean bIsStable;
	private int iMassNumber;
	private BigDecimal bdMass;
	private BigDecimal bdAbundance;

	private IsotopeType(AtomType a_atomType, int a_iMassNum, boolean a_isStable, String a_mass, String a_abundance ) {
		this.atomType = a_atomType;
		this.bIsStable = a_isStable;
		this.iMassNumber = a_iMassNum;
		this.bdMass = new BigDecimal(a_mass);
		this.bdAbundance = new BigDecimal(a_abundance);
	}

	@Override
	public String getName() {
		return this.name();
	}

	/**
	 * Return the chemical symbol of the atom represented by this isotope.
	 */
	@Override
	public String getSymbol() {
		return this.atomType.getSymbol()+"^"+this.iMassNumber;
	}

	@Override
	public String getAtomSymbol() {
		return this.atomType.getSymbol();
	}

	/**
	 * Return the mass number.
	 */
	public int getMassNumber() {
		return this.iMassNumber;
	}

	/**
	 * Return <code>true</code> if the isotope is stable.
	 */
	public boolean isStable() {
		return this.bIsStable;
	}

	@Override
	public BigDecimal getMass() {
		return this.bdMass;
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.bdMass;
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.bdMass;
	}

	public BigDecimal getAbundance() {
		return this.bdAbundance;
	}

	public static IsotopeType forAtomTypeWithMassNumber(AtomType a_atom, int a_iMassNum) {
		for ( IsotopeType e : IsotopeType.values() ) {
			if ( e.atomType != a_atom )
				continue;
			if ( e.iMassNumber != a_iMassNum )
				continue;
			return e;
		}
		logger.error("No isotope for the atom {} with mass number {}.", a_atom.getSymbol(), a_iMassNum);
		return null;
	}

	private static final Map<AtomType, List<IsotopeType>> mapAtomToIsotopes = new HashMap<>();
	private static final Map<AtomType, IsotopeType> mapAtomToMainIsotope = new HashMap<>();

	public static List<IsotopeType> getAllIsotopes(AtomType a_atom) {
		if ( mapAtomToIsotopes.containsKey(a_atom) )
			return mapAtomToIsotopes.get(a_atom);

		List<IsotopeType> lIsotopes = new ArrayList<>();
		for ( IsotopeType e : IsotopeType.values() )
			if ( e.atomType == a_atom )
				lIsotopes.add(e);

		mapAtomToIsotopes.put(a_atom, lIsotopes);

		return lIsotopes;
	}

	public static IsotopeType getMainIsotope(AtomType a_atom) {
		if ( mapAtomToMainIsotope.containsKey(a_atom) )
			return mapAtomToMainIsotope.get(a_atom);

		IsotopeType isoMain = null;
		for ( IsotopeType e : getAllIsotopes(a_atom) ) {
			if ( isoMain == null ) {
				isoMain = e;
				continue;
			}
			// e > isoMain
			if ( e.bdAbundance.compareTo(isoMain.bdAbundance) > 0 )
				isoMain = e;
		}
		if ( isoMain == null )
			logger.warn("No isotope for the atom {}", a_atom.getSymbol());

		mapAtomToMainIsotope.put(a_atom, isoMain);
		return isoMain;
	}

}
