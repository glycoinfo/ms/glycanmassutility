package org.glycoinfo.ms.GlycanMassUtility.dict.molecule;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;
import org.glycoinfo.ms.GlycanMassUtility.om.Molecule;

public enum DerivatizationType implements IMassElement {

	Me( "methyl",       "CH3"),
	DMe("dmethyl",      "CD3"),
	Ac( "acetyl",       "C2H3O"),
	DAc("dacetyl",      "C2D3O"),
	HMe("heavy methyl", "C^13H3"),
	;

	private String name;
	private Molecule molecule;

	private DerivatizationType(String a_name, String strFormula) {
		this.name = a_name;
		this.molecule = Molecule.parse(strFormula);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public Molecule getMolecule() {
		return this.molecule;
	}

	@Override
	public BigDecimal getMass() {
		return this.molecule.getMass();
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.molecule.getMonoisotopicMass();
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.molecule.getAverageMass();
	}
}
