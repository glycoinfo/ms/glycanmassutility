package org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent;

import java.util.ArrayList;
import java.util.List;

public enum ReducingEndList {
	_redEnd(null,  "H2O",         3, 3, "Reduced reducing end"),
	_PA    ("2AP", "C5H6N2",      3, 2, "2-Aminopyridine"),
	_2AB   (null,  "C7H8N2O",     3, 2, "2-Aminobenzamide"),
	_AA    (null,  "C7H7NO2",     5, 2, "Anthranilic Acid"),
	_DAP   (null,  "C5H7N3",      4, 2, "2,6-Diaminopyridine"),
	_4AB   (null,  "C7H9N3",      5, 3, "4-Aminobenzamidine"),
	_DAPMAB(null,  "C14H13N7O2",  5, 2, "4-(N-[2,4-Diamino-6-pteridinylmethyl]amino)benzoic acid"),
	_AMC   (null,  "C10H9NO2",    7, 3, "7-Amino-4-methylcoumarin"),
	_6AQ   (null,  "C9H8N2",      3, 2, "6-Aminoquinoline"),
	_2AAc  (null,  "C13H10N2O",   3, 2, "2-Aminoacridone"),
	_FMC   (null,  "C15H14N2O2",  4, 2, "9-Fluorenylmethyl carbazate"),
	_DH    (null,  "C12H15N3O2S", 4, 2, "Dansylhydrazine"),
	_BOA   (null,  "C7H9NO",      3, 3, "O-benzylhydroxylamine"),
	;

	private SubstituentType type;

	private ReducingEndList(String strSynonyms, String strComp,
			int nMe, int nAc, String strDescription ) {
		// Parse sysnonyms
		List<String> lSynonyms = new ArrayList<>();
		if ( strSynonyms != null )
			for ( String strSynonym : strSynonyms.split(",") )
				lSynonyms.add(strSynonym);

		// Set name without first "_"
		this.type = new SubstituentType(this.name().substring(1), lSynonyms,
				strComp, nMe, false, nAc, false, 1, false, true, strDescription);
	}

	public SubstituentType getType() {
		return type;
	}
}
