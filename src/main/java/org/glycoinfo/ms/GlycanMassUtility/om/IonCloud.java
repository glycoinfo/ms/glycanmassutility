package org.glycoinfo.ms.GlycanMassUtility.om;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.glycoinfo.ms.GlycanMassUtility.dict.generic.GenericMassElementDictionary;
import org.glycoinfo.ms.GlycanMassUtility.dict.molecule.IonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IonCloud implements IIonElement {

	private static final Logger logger = LoggerFactory.getLogger(IonCloud.class);

	private static Pattern pattern = Pattern.compile("([+|-]?)([0-9]*)([A-Z][0-9a-zA-Z]*)");

	/**
	 * Create a new IonCloud instance from the ion representations like followings:
	 * <p>
	 * Na<br>
	 * Na+K<br>
	 * 2Na-2H<br>
	 * </p>
	 * Each ion element must be specified in {@link IonType} or manually added
	 * using {@link GenericMassElementDictionary#addGenericIon(String, String, int)}.
	 * @param init String of the ion representations
	 * @return IonCloud 
	 */
	public static IonCloud parse(String init) throws IllegalArgumentException {
		boolean bHasError = false;
		String strErrMsg = null;
		List<String> lErrMsg = new ArrayList<>();

		int cur = 0;

		IonCloud ionCloud = new IonCloud();

		Matcher m = pattern.matcher(init);
		while (m.find()) {
			String strSign = m.group(1);
			String strCount = m.group(2);
			String strIonElement = m.group(3);
			cur = m.end();

			// Try to find from IonType
			IIonElement ion = IonType.forName(strIonElement);

			// Try to seek generic element dictionary if undefined in IonType
			if ( ion == null ) {
				ion = GenericMassElementDictionary.getIon(strIonElement);
				if ( ion == null ) {
					bHasError = true;
					strErrMsg = "Invalid ion element: "+strIonElement;
					logger.error(strErrMsg);
					lErrMsg.add(strErrMsg);
					continue;
				}
			}

			int mul = ( strSign.equals("-") )? -1 : 1;

			if ( strCount != null && !strCount.isEmpty() )
				mul *= Integer.valueOf(strCount);

			ionCloud.addIons(ion, mul);
			ionCloud.addMass(ion, mul);
			ionCloud.addCharges(ion, mul);
		}

		if ( cur != init.length() ) {
			bHasError = true;
			strErrMsg = "Invalid ion format: "+init;
			logger.error(strErrMsg);
			lErrMsg.add(strErrMsg);
		}
		if ( bHasError ) {
			if (lErrMsg.size() > 2) {
				lErrMsg = lErrMsg.subList(0, 2);
				lErrMsg.add("...");
			}
			throw new IllegalArgumentException(String.join("\n", lErrMsg));
		}

		return ionCloud;
	}

	private TreeMap<IIonElement, Integer> mapIonToCount;
	private int nTotalIons;
	private int iTotalCharges;
	private BigDecimal bdTotalMassMain;
	private BigDecimal bdTotalMassAvg;

	public IonCloud() {
		this.mapIonToCount = new TreeMap<>(IMassElement.NameComparator);
		this.nTotalIons = 0;
		this.iTotalCharges = 0;
		this.bdTotalMassMain = BigDecimal.ZERO;
		this.bdTotalMassAvg = BigDecimal.ZERO;
	}

	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public BigDecimal getMass() {
		return this.bdTotalMassMain;
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		return this.bdTotalMassMain;
	}

	@Override
	public BigDecimal getAverageMass() {
		return this.bdTotalMassAvg;
	}

	@Override
	public int getCharges() {
		return this.iTotalCharges;
	}

	public int getIonsCount() {
		return this.nTotalIons;
	}

	/**
	 * Return a string representation of this object
	 */
	public String toString() {
		// first positives then negatives
		StringBuilder sbPos  = new StringBuilder();
		StringBuilder sbNeg  = new StringBuilder();
		for (Map.Entry<IIonElement, Integer> entry : this.mapIonToCount.entrySet()) {
			// For positives
			if (entry.getValue() > 0) {
				if (sbPos.length() > 0)
					sbPos.append('+');
				if (entry.getValue() > 1)
					sbPos.append(entry.getValue());
				sbPos.append(entry.getKey().getName());
				continue;
			}
			// For negatives
			if (entry.getValue() == -1)
				sbNeg.append('-');
			else
				sbNeg.append(entry.getValue());
			sbNeg.append(entry.getKey().getName());
		}

		StringBuilder sb = sbPos.append(sbNeg);
		if (sb.length() == 0)
			sb.append('0');

		return sb.toString();
	}

	/**
	 * Create a copy of the object.
	 */
	public IonCloud clone() {
		IonCloud ret = new IonCloud();

		for (Map.Entry<IIonElement, Integer> c : this.mapIonToCount.entrySet())
			ret.mapIonToCount.put(c.getKey(), c.getValue());

		ret.iTotalCharges   = this.iTotalCharges;
		ret.nTotalIons      = this.nTotalIons;
		ret.bdTotalMassMain = this.bdTotalMassMain;
		ret.bdTotalMassAvg  = this.bdTotalMassAvg;

		return ret;
	}

	private void addIons(IIonElement e, int num) {
		if ( e == null || num == 0 )
			return;
		Integer cur_num = num;
		if ( this.mapIonToCount.containsKey(e) ) {
			cur_num += this.mapIonToCount.get(e);
			// Remove element when num becomes zero
			if ( cur_num == 0 ) {
				this.mapIonToCount.remove(e);
				return;
			}
		}
		this.mapIonToCount.put(e, cur_num);
	}

	private void addMass(IMassElement e, int num) {
		if ( e == null || num == 0 )
			return;
		BigDecimal bdNum = new BigDecimal(num);
		this.bdTotalMassMain = this.bdTotalMassMain.add( e.getMonoisotopicMass().multiply( bdNum ) );
		this.bdTotalMassAvg = this.bdTotalMassAvg.add( e.getAverageMass().multiply( bdNum ) );
	}

	private void addCharges(IIonElement e, int num) {
		this.iTotalCharges += e.getCharges() * num;
		this.nTotalIons += num;
	}

	public IonCloud addIonClouds(IonCloud i, int num) {
		if ( i == null)
			return null;

		IonCloud ret = this.clone();
		if ( num == 0 )
			return ret;

		for ( Map.Entry<IIonElement, Integer> a : i.mapIonToCount.entrySet() )
			ret.addIons(a.getKey(), num * a.getValue());

		ret.addCharges(i, num);
		ret.addMass(i, num);

		return ret;
	}
}
