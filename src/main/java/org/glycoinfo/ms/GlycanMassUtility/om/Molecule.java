package org.glycoinfo.ms.GlycanMassUtility.om;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.glycoinfo.ms.GlycanMassUtility.dict.atom.AtomType;
import org.glycoinfo.ms.GlycanMassUtility.dict.atom.IAtom;
import org.glycoinfo.ms.GlycanMassUtility.dict.atom.IsotopeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An object class for molecule. Using {@link #parse(String)} allows to
 * create an instance from the chemical formula. Any atoms and its isotopes are
 * defined in {@link AtomType} and {@link IsotopeType}, respectively.
 * @see AtomType
 * @see IsotopeType
 * @author Masaaki Matsubara
 *
 */
public class Molecule implements IIonElement {

	private static final Logger logger = LoggerFactory.getLogger(Molecule.class);

	private static Pattern patMol = Pattern.compile("([A-Z][a-z]?)([0-9]*)(\\^[0-9]+)?");

	/**
	 * Return a new Molecule from its chemical formula.
	 * @param init String of the chemical formula
	 * @return Molecule
	 * @throws Exception
	 */
	public static Molecule parse(String init) {
		boolean bHasError = false;
		String strErrMsg = null;
		List<String> lErrMsg = new ArrayList<>();

		if (init.equals("0"))
			init = "";

		int mul = 1;
		if (init.startsWith("-")) {
			mul = -1;
			init = init.substring(1);
		}

		Molecule mol = new Molecule();

		int cur = 0;
		Matcher m = patMol.matcher(init);
		while (m.find()) {
			String strAtomSymbol = m.group(1);
			String strAtomCount = m.group(2);
			String strMassNumber = m.group(3);
			cur = m.end();

			// Find atom
			IAtom atom = AtomType.forSymbol(strAtomSymbol);
			if ( atom == null ) {
				bHasError = true;
				strErrMsg = "Invalid atom symbol: "+strAtomSymbol;
				logger.error(strErrMsg);
				lErrMsg.add(strErrMsg);
				continue;
			}

			// Try to find isotope
			if (strMassNumber != null) {
				strMassNumber = strMassNumber.substring(1, strMassNumber.length());
				atom = IsotopeType.forAtomTypeWithMassNumber((AtomType)atom, Integer.valueOf(strMassNumber));
			}
			if ( atom == null ) {
				bHasError = true;
				strErrMsg = "Invalid isotope: "+strMassNumber+strAtomSymbol;
				logger.error(strErrMsg);
				lErrMsg.add(strErrMsg);
				continue;
			}

			int mulAtom = mul;
			if ( strAtomCount != null && !strAtomCount.isEmpty() )
				mulAtom *= Integer.valueOf(strAtomCount);

			mol.addInAtoms(atom, mulAtom);
			mol.addInMass(atom, mulAtom);
		}

		int charges = 0;
		int i = cur;
		for (; i < init.length(); i++) {
			if (init.charAt(i) == '-')
				charges--;
			else if (init.charAt(i) == '+')
				charges++;
			else
				break;
		}
		if (i != init.length()) {
			bHasError = true;
			strErrMsg = "Invalid format: " + init;
			logger.error(strErrMsg);
			lErrMsg.add(strErrMsg);
		}

		if ( bHasError ) {
			if (lErrMsg.size() > 2) {
				lErrMsg = lErrMsg.subList(0, 2);
				lErrMsg.add("...");
			}
			throw new IllegalArgumentException(String.join("\n", lErrMsg));
		}

		if (charges != 0) {
			mol.iCharges = charges;
			// negative charges will add mass and positive remove mass (obvious but worth
			// stating)
			mol.addInMass(AtomType.e, -charges);
		}

		return mol;
	}

	private TreeMap<IAtom, Integer> mapAtomToCount;
	private BigDecimal bdMainMass;
	private BigDecimal bdAvgMass;
	private int iCharges;

	/**
	 * Create an empty molecule.
	 */
	public Molecule() {
		// Set tree map with comparator
		mapAtomToCount = new TreeMap<>(IAtom.SymbolComparator);
		bdMainMass = BigDecimal.ZERO;
		bdAvgMass = BigDecimal.ZERO;
		iCharges = 0;
	}

	@Override
	public String getName() {
		return this.toString();
	}

	public BigDecimal getMass() {
		return this.bdMainMass;
	}

	public BigDecimal getMonoisotopicMass() {
		return this.bdMainMass;
	}

	public BigDecimal getAverageMass() {
		return this.bdAvgMass;
	}

	public int getCharges() {
		return this.iCharges;
	}

	/**
	 * Return {@code true} if the two molecules have the same chemical formula.
	 */
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Molecule))
			return false;
		return this.toString().equals(other.toString());
	}
	/**
	 * Return an hash code associated with this molecule.
	 */
	public int hashCode() {
		return this.toString().hashCode();
	}

	/**
	 * Create a new object which is a copy of the current one.
	 */
	public Molecule clone() {
		Molecule ret = new Molecule();
		ret.mapAtomToCount = (TreeMap<IAtom, Integer>) this.mapAtomToCount.clone();
		ret.bdMainMass = this.bdMainMass.add(BigDecimal.ZERO);
		ret.bdAvgMass = this.bdAvgMass.add(BigDecimal.ZERO);
		ret.iCharges = this.iCharges;
		return ret;
	}

	/**
	 * Return the chemical formula of the molecule.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<IAtom, Integer> a : this.mapAtomToCount.entrySet()) {
			IAtom atom = a.getKey();
			sb.append(atom.getAtomSymbol());
			if ( a.getValue() != 1 )
				sb.append(a.getValue().toString());
			if ( atom instanceof IsotopeType )
				sb.append("^").append( ((IsotopeType)atom).getMassNumber() );
		}
		for (int i = 0; i < Math.abs(this.iCharges); i++) {
			if (this.iCharges > 0)
				sb.append('+');
			else
				sb.append('-');
		}
		return sb.toString();
	}

	public Map<IAtom, Integer> getRawAtomTree() {
		return this.mapAtomToCount;
	}

	/**
	 * Add {@code num} instances of atom {@code a} to the molecule.
	 */
	private void addInAtoms(IAtom a, int num) {
		if ( a == null || num == 0 )
			return;
		Integer cur_num = num;
		if ( this.mapAtomToCount.containsKey(a) ) {
			cur_num += this.mapAtomToCount.get(a);
			// Remove element when num becomes zero
			if ( cur_num == 0 ) {
				this.mapAtomToCount.remove(a);
				return;
			}
		}
		this.mapAtomToCount.put(a, cur_num);
	}

	private void addInMass(IMassElement e, int num) {
		if ( e == null || num == 0 )
			return;
		BigDecimal bdNum = new BigDecimal(num);
		this.bdMainMass = this.bdMainMass.add( e.getMonoisotopicMass().multiply( bdNum ) );
		this.bdAvgMass = this.bdAvgMass.add( e.getAverageMass().multiply( bdNum ) );
	}

	/**
	 * Return a new Molecule which added {@code num} times the content of
	 * molecule {@code m} to the molecule.
	 * @param m a Molecule to be added {@code num} times
	 * @param num count of addition
	 * @return A new Molecule object resulting this addition
	 */
	public Molecule addMolecules(Molecule m, int num) {
		if ( m == null)
			return null;

		Molecule ret = this.clone();
		if ( num == 0 )
			return ret;

		for ( Map.Entry<IAtom, Integer> a : m.mapAtomToCount.entrySet() )
			ret.addInAtoms(a.getKey(), num * a.getValue());
		ret.iCharges += num * m.iCharges;

		ret.addInMass(m, num);

		return ret;
	}

	public Molecule removeMolecules(Molecule m, int num) {
		return addMolecules(m, -num);
	}
}
