package org.glycoinfo.ms.GlycanMassUtility.om;

public interface IIonElement extends IMassElement {

	public int getCharges();
}
