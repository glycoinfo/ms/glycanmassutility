package org.glycoinfo.ms.GlycanMassUtility.om;

import java.math.BigDecimal;
import java.util.Comparator;

public interface IMassElement {

	public static final Comparator<IMassElement> NameComparator = new Comparator<IMassElement>() {
		@Override
		public int compare(IMassElement o1, IMassElement o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};

	public String getName();
	/**
	 * Return the mass of this element.
	 */
	public BigDecimal getMass();

	/**
	 * Return the mono-isotopic mass.
	 */
	public BigDecimal getMonoisotopicMass();

	/**
	 * Return the average mass.
	 */
	public BigDecimal getAverageMass();

}
