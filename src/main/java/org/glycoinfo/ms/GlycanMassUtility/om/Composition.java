package org.glycoinfo.ms.GlycanMassUtility.om;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.glycoinfo.ms.GlycanMassUtility.dict.molecule.NeutralType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.ResidueType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.FragmentDictionary;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideDictionary;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.SubstituentDictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Composition implements IMassElement {

	private static final Logger logger = LoggerFactory.getLogger(Composition.class);

	/**
	 * Return a new Composition from its monosaccharide composition format.
	 * @param init String of the monosaccharide composition format
	 * @return Composition
	 * @throws Exception
	 */
	public static Composition parse(String init) throws IllegalArgumentException {
		boolean bHasError = false;
		String strErrMsg = "";
		List<String> lErrMsg = new ArrayList<>();

		Composition compo = new Composition();

		// Parse string
		init = init.trim();
		String[] tokens = init.split("\\)");
		for ( String token : tokens ) {
			if (token.isEmpty())
				continue;
			String[] res = token.split("\\(");
			String name = res[0];
			String count = (res.length == 1)? "1" : res[1];

			ResidueType type = null;
			if ( FragmentDictionary.findFragmentType(name) )
				type = (ResidueType)FragmentDictionary.getFragmentType(name);
			if ( MonosaccharideDictionary.findMonosaccharide(name) )
				type = MonosaccharideDictionary.getMonosaccharide(name);
			if ( SubstituentDictionary.findSubstituent(name) )
				type = SubstituentDictionary.getSubstituent(name);
			if ( SubstituentDictionary.findReducingEnd(name) )
				type = SubstituentDictionary.getReducingEnd(name);
			if ( type == null ) {
				bHasError = true;
				strErrMsg = "The residue name \""+name+"\" is not in the dictionary.";
				logger.error(strErrMsg);
				lErrMsg.add(strErrMsg);
			}

			int n = 0;
			try {
				n = Integer.valueOf(count);
			} catch (NumberFormatException e) {
				bHasError = true;
				strErrMsg = "Invalid count: "+count;
				logger.error(strErrMsg);
				lErrMsg.add(strErrMsg);
			}
			if ( type != null && n != 0 )
				compo.setResidues(type, n);
		}

		if ( bHasError ) {
			if ( lErrMsg.size() > 2 ) {
				lErrMsg = lErrMsg.subList(0, 2);
				lErrMsg.add("...");
			}
			throw new IllegalArgumentException(String.join("\n", lErrMsg));
		}

		return compo;
	}

	private Map<ResidueType, Integer> mapResTypeToCount;

	protected BigDecimal m_dMonoisotopicMass;
	protected BigDecimal m_dAverageMass;

	public Composition() {
		this.mapResTypeToCount = new TreeMap<>();

		this.m_dMonoisotopicMass = null;
		this.m_dAverageMass = null;
	}

	/**
	 * Adds a ResidueType into this composition.
	 */
	public void addResidue(ResidueType type) {
		if ( !this.mapResTypeToCount.containsKey(type) )
			this.mapResTypeToCount.put(type, 0);
		int count = this.mapResTypeToCount.get(type);
		this.mapResTypeToCount.put(type, ++count);
		resetMass();
	}

	/**
	 * Returns all ResidueTypes in this composition.
	 * @return ResidueTypes in this composition
	 */
	public Set<ResidueType> getResidueTypes() {
		return this.mapResTypeToCount.keySet();
	}

	/**
	 * Set residue with count. The count of residues with the type
	 * will be reset.
	 * @param type ResidueType to set
	 * @param count int of residue count
	 */
	public void setResidues(ResidueType type, int count) {
		if ( count == 0 )
			this.mapResTypeToCount.remove(type);
		else
			this.mapResTypeToCount.put(type, count);
		resetMass();
	}

	/**
	 * Returns count for the specified residue type.
	 * @param type
	 * @return
	 */
	public int getResidueCount(ResidueType type) {
		if ( !this.mapResTypeToCount.containsKey(type) )
			return 0;
		return this.mapResTypeToCount.get(type);
	}

	public int getTotalResidueCount() {
		int countTotal = 0;
		for ( int count : this.mapResTypeToCount.values() )
			countTotal += count;
		return countTotal;
	}

	private Molecule computeMolecule() {
		Molecule molAll = new Molecule();
		int nTotalRes = 0;
		for (ResidueType type : this.mapResTypeToCount.keySet()) {
			int count = this.mapResTypeToCount.get(type);
			Molecule mol = Molecule.parse(type.getComposition());
			molAll = molAll.addMolecules(mol, count);
			nTotalRes += count;
		}
		// Subtract waters for each glycosidic bond
		molAll = molAll.addMolecules(
				NeutralType.Water.getMolecule(),
				1 - nTotalRes
			);
		return molAll;
	}

	public String getAtomicComposition() {
		return computeMolecule().toString();
	}

	private void resetMass() {
		this.m_dMonoisotopicMass = null;
		this.m_dAverageMass = null;
	}

	@Override
	public String getName() {
		return this.toString();
	}

	@Override
	public BigDecimal getMass() {
		return this.getMonoisotopicMass();
	}

	@Override
	public BigDecimal getMonoisotopicMass() {
		if ( this.m_dMonoisotopicMass == null )
			computeMass();
		return this.m_dMonoisotopicMass;
	}

	@Override
	public BigDecimal getAverageMass() {
		if ( this.m_dAverageMass == null )
			computeMass();
		return this.m_dAverageMass;
	}

	private void computeMass() {
		Molecule mol = computeMolecule();
		this.m_dMonoisotopicMass = mol.getMonoisotopicMass();
		this.m_dAverageMass = mol.getAverageMass();
	}

	@Override
	public String toString() {
		List<String> lNames = new ArrayList<>();
		for ( ResidueType type : this.mapResTypeToCount.keySet() ) {
			int count = this.mapResTypeToCount.get(type);
			lNames.add(type.getName()+"("+count);
		}
		return String.join(")", lNames)+")";
	}

	@Override
	public boolean equals(Object obj) {
		if ( super.equals(obj) )
			return true;
		if ( !(obj instanceof Composition) )
			return false;
		return this.toString().equals( ((Composition)obj).toString() );
	}

	/**
	 * Merge two or more compositions into a composition.
	 * @param compositions
	 * @return Merged composition
	 */
	public static Composition merge(Composition... compositions) {
		// Map type to count
		Map<ResidueType, Integer> mapMSTypeToCount = new TreeMap<>();
		for ( Composition compo : compositions ) {
			for ( ResidueType type : compo.mapResTypeToCount.keySet() ) {
				int count = compo.mapResTypeToCount.get(type);
				if ( !mapMSTypeToCount.containsKey(type) ) {
					mapMSTypeToCount.put(type, count);
					continue;
				}
				count += mapMSTypeToCount.get(type);
				mapMSTypeToCount.put(type, count);
			}
		}
		// Trim types with 0 count
		Composition compoTotal = new Composition();
		for ( ResidueType type : mapMSTypeToCount.keySet() ) {
			int count = mapMSTypeToCount.get(type);
			if ( count == 0 )
				continue;
			compoTotal.mapResTypeToCount.put(type, count);
		}
		return compoTotal;
	}


}