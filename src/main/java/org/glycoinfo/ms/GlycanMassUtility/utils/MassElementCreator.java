package org.glycoinfo.ms.GlycanMassUtility.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.ms.GlycanMassUtility.dict.molecule.NeutralType;
import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;

public class MassElementCreator {
	/**
	 * Merge given mass elements into an element. Each element is connected by dehydration (water loss).
	 * @param elements IMassElement to be merged
	 * @return IMassElement 
	 */
	public static IMassElement createComplexMassElement(List<IMassElement> elements) {
		List<String> names = new ArrayList<>();
		BigDecimal massMonoisotopic = BigDecimal.ZERO;
		BigDecimal massAverage = BigDecimal.ZERO;
		IMassElement water = NeutralType.Water;
		for ( IMassElement element : elements ) {
			names.add( element.getName() );
			// remove water for each connection
			if ( !massMonoisotopic.equals( BigDecimal.ZERO ) ) {
				massMonoisotopic = massMonoisotopic.subtract( water.getMonoisotopicMass() );
				massAverage = massAverage.subtract( water.getAverageMass() );
			}
			massMonoisotopic = massMonoisotopic.add( element.getMonoisotopicMass() );
			massAverage = massAverage.add( element.getAverageMass() );
		}
		return createMassElement(String.join(" + ", names), massMonoisotopic, massAverage);
	}

	public static IMassElement createMassElement(String name, BigDecimal massMonoisotopic, BigDecimal massAverage) {
		return new IMassElement() {
			@Override
			public String getName() {
				return name;
			}

			@Override
			public BigDecimal getMass() {
				return massMonoisotopic;
			}

			@Override
			public BigDecimal getMonoisotopicMass() {
				return massMonoisotopic;
			}

			@Override
			public BigDecimal getAverageMass() {
				return massAverage;
			}
		};
	}

}
