package org.glycoinfo.ms.GlycanMassUtility.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.glycoinfo.ms.GlycanMassUtility.om.IIonElement;
import org.glycoinfo.ms.GlycanMassUtility.om.IMassElement;

public class MassUtils {

	public static BigDecimal computeMass(IMassElement... a_elems) {
		BigDecimal bdMass = BigDecimal.ZERO;
		for ( IMassElement elem : a_elems )
			bdMass = bdMass.add( elem.getMass() );
		return bdMass;
	}

	public static BigDecimal computeMz(IMassElement... a_elems) {
		int iCharges = 0;
		for ( IMassElement elem : a_elems )
			if ( elem instanceof IIonElement )
				iCharges += ((IIonElement)elem).getCharges();
		if ( iCharges == 0 )
			return null;

		BigDecimal bdMass = computeMass(a_elems);
		return bdMass.divide( new BigDecimal(iCharges), 10, RoundingMode.HALF_UP );
	}

}
