package org.glycoinfo.ms.GlycanMassUtility.utils;

import org.glycoinfo.ms.GlycanMassUtility.om.Molecule;

public class MoleculeGenerator {

	public static Molecule fromCarbonLength(int a_iLength) {
		Molecule molMS = new Molecule();
		molMS = molMS.addMolecules( Molecule.parse("CH2O") , a_iLength);

		return molMS;
	}

	public static Molecule fromCrossRingCleaved(
			int a_iLength, int a_iAnomPos,
			int a_iStartClvPos, int a_iEndClvPos,
			boolean a_bHasAnom) {

		if ( a_iStartClvPos > a_iEndClvPos ) {
			int tmp = a_iStartClvPos;
			a_iStartClvPos = a_iEndClvPos;
			a_iEndClvPos = tmp;
		}
		if ( a_iStartClvPos < 0 || a_iLength < a_iEndClvPos )
			return null;

		boolean isInside = (a_iStartClvPos < a_iAnomPos) ^ a_bHasAnom;
		boolean[] aRemains = new boolean[a_iLength];
		for ( int i=1; i<=a_iLength; i++ ) {
			if ( i > a_iStartClvPos && i < a_iEndClvPos )
			aRemains[i-1] = (isInside)? true : false;
		}

		return null;
	}
}
