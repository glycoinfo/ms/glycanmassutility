package org.glycoinfo.ms.GlycanMassUtility.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.CrossRingCleavageType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.FragmentDictionary;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.GlycosidicCleavageType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.ICleavageType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.fragment.IFragmentType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideList;

public class TestFragments {

	public static void main(String[] args) {
		List<ICleavageType> lClvTypes = new ArrayList<>();
		lClvTypes.addAll( Arrays.asList(CrossRingCleavageType.values()) );
		lClvTypes.addAll( Arrays.asList(GlycosidicCleavageType.values()) );
		for (MonosaccharideList list : MonosaccharideList.values()) {
			for (ICleavageType clvType : lClvTypes) {
                List<IFragmentType> lFragments = FragmentDictionary.getFragments(list.getType(), clvType);
                if (lFragments == null)
                    continue;
                for (IFragmentType type : lFragments)
                    checkMass(type);
			}
		}
	}

	private static void checkMass(IFragmentType type) {
        System.out.println(type.getName() + ": " + type.getMonoisotopicMass());
    }

}
