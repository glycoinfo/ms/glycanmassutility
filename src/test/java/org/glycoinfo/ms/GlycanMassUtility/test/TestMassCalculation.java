package org.glycoinfo.ms.GlycanMassUtility.test;

import org.glycoinfo.ms.GlycanMassUtility.om.Composition;
import org.glycoinfo.ms.GlycanMassUtility.om.IonCloud;
import org.glycoinfo.ms.GlycanMassUtility.utils.MassUtils;

public class TestMassCalculation {

	public static void main(String[] args) {
		Composition compo = Composition.parse("HexNAc(2)Hex(3)");
		IonCloud ion = IonCloud.parse("Na");
		checkContent(compo, ion);
		ion = IonCloud.parse("2Na");
		checkContent(compo, ion);

		compo = Composition.parse("HexNAc(2)Hex(3)Neu5Ac(1)");
		ion = IonCloud.parse("-H");
		checkContent(compo, ion);
		ion = IonCloud.parse("Na-H");
		checkContent(compo, ion);
	}

	private static void checkContent(Composition compo, IonCloud ion) {
		System.out.println("Name: " + compo.getName());
		System.out.println("Monoisotopic: " + compo.getMonoisotopicMass());
		System.out.println("Average: " + compo.getAverageMass());
		System.out.println("Ion: " + ion.getName());
		System.out.println("Monoisotopic: " + ion.getMonoisotopicMass());
		System.out.println("Average: " + ion.getAverageMass());
		System.out.println("Charge: " + ion.getCharges());
		System.out.println("Mass: " + MassUtils.computeMass(compo, ion));
		System.out.println("m/z: " + MassUtils.computeMz(compo, ion));
	}
}
