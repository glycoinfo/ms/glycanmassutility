package org.glycoinfo.ms.GlycanMassUtility.test;

import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideList;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.SubstituentList;
import org.glycoinfo.ms.GlycanMassUtility.om.Composition;

public class TestComposition {

	public static void main(String[] args) {
		Composition compo = new Composition();
		compo.addResidue(SubstituentList.Ac.getType());
		checkContent(compo);

		compo = Composition.parse("HexNAc(2)Hex(3)");
		checkContent(compo);
		// Add
		compo.addResidue(MonosaccharideList._Hex.getType());
		checkContent(compo);
		compo.addResidue(MonosaccharideList._Hex.getType());
		compo.addResidue(MonosaccharideList._Hex.getType());
		compo.addResidue(MonosaccharideList._Hex.getType());
		checkContent(compo);
		// Set
		compo.setResidues(MonosaccharideList._Hex.getType(), 2);
		checkContent(compo);
		compo.setResidues(MonosaccharideList._Hex.getType(), 0);
		checkContent(compo);
		// Merge
		Composition compoToMerge = Composition.parse("dHex(2)");
		compo = Composition.merge(compo, compoToMerge);
		checkContent(compo);

		// Fragments
		Composition compoFragment = Composition.parse("HexNAc#x_1_4(1)");
		compo = Composition.merge(compo, compoFragment);
		checkContent(compo);

		// substituents
		compoToMerge = Composition.parse("Ac(1)");
		compo = Composition.merge(compo, compoToMerge);
		checkContent(compo);
	}

	private static void checkContent(Composition compo) {
		System.out.println("Name: "+compo.getName());
		System.out.println("Monoisotopic: "+compo.getMonoisotopicMass());
		System.out.println("Average: "+compo.getAverageMass());
		System.out.println("AtomicComposition: "+compo.getAtomicComposition());
	}
}
