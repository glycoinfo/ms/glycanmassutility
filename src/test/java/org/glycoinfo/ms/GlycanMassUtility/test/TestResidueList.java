package org.glycoinfo.ms.GlycanMassUtility.test;

import java.math.BigDecimal;

import org.glycoinfo.ms.GlycanMassUtility.dict.residue.ResidueType;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideDictionary;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.monosaccharide.MonosaccharideList;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.ReducingEndList;
import org.glycoinfo.ms.GlycanMassUtility.dict.residue.substituent.SubstituentList;

public class TestResidueList {

	public static void main(String[] args) {
		for ( MonosaccharideList list : MonosaccharideList.values() )
			checkMass(list.getType());
		for ( SubstituentList list : SubstituentList.values() )
			checkMass(list.getType());
		for ( ReducingEndList list : ReducingEndList.values() )
			checkMass(list.getType());
	}

	public static void checkMass(ResidueType type) {
		BigDecimal mass = type.getMonoisotopicMass();
		System.out.println(type.getName()+": "+mass);
		if ( type.getSynonyms() == null || type.getSynonyms().isEmpty() )
			return;
		for ( String synonym : type.getSynonyms() )
			System.out.print( synonym+": "+MonosaccharideDictionary.findMonosaccharide(synonym) );
		System.out.println();
	}
}
