# GlycanMassUtility

An utility for calculating mass of glycan compositions.

## Features

* Calculate masses for monosaccharide compositions
  * Byonic style notation can be handled
* Some modifications at reducing end (e.g. PA) can be specified

## Requirement

* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher

## Build

```sh
$ mvn clean install
```

## Install

Copy and paste this inside your `pom.xml` `dependencies` block.

```xml
<dependency>
  <groupId>org.glycoinfo.ms</groupId>
  <artifactId>GlycanMassUtility</artifactId>
  <version>1.0.0</version>
</dependency>
```

If you do not do registry setup yet, you need to add following into your `pom.xml`.

```xml
<repositories>
  <repository>
    <id>GlycanMassUtility-gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/43695885/packages/maven</url>
  </repository>
</repositories>
```

## Usage

To see information for a simple glycan composition writen in Byonic style:

```java
import org.glycoinfo.ms.GlycanMassUtility.om.Composition;
import org.glycoinfo.ms.GlycanMassUtility.om.IonCloud;
import org.glycoinfo.ms.GlycanMassUtility.utils.MassUtils;

public class TestComposition {

	public static void main(String[] args) {
		// Create composition
		Composition compo = Composition.parse("HexNAc(2)Hex(3)");

		System.out.println("Name: "+compo.getName());
		// -> HexNAc(2)Hex(3)

		System.out.println("Monoisotopic: "+compo.getMonoisotopicMass());
		// -> 910.327780038

		System.out.println("Average: "+compo.getAverageMass());
		// -> 910.823618

		System.out.println("AtomicComposition: "+compo.getAtomicComposition());
		// -> C34H58N2O26

		// Create ion
		IonCloud ion = IonCloud.parse("Na");

		// Calculate m/z value
		double mz = MassUtils.computeMz(compo, ion);
		System.out.println("[M+Na]+ (m/z): "+mz);
		// -> 933.317001108
	}

```

## Author

#### Masaaki Matsubara
* The Noguchi Institute, Japan

#### Issaku Yamada
* The Noguchi Institute, Japan

## Release note
* 2024/09/19: changed revision to 1.2.1
  * Fixed error in m/z value calculation
* 2024/09/14: changed version to 1.2.0
  * Handled monosaccharide cross ring cleavage
* 2024/08/06: changed version to 1.1.0
  * Updated parse methods to throw exception
  * Added class to create custom and complex MassElement
* 2024/06/27: released ver 1.0.0
